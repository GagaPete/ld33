module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-htmlhint');
 
    grunt.initConfig ({
        coffee: {
            compile: {
                options: {
                    join: true
                },
                files: {
                    "src/main.js": ["src/coffee/*.coffee"]
                }
            }
        },

        uglify: {
            min: {
                files: {
                    'src/main.min.js' : ['src/main.js']
                }
            }
        },
 
        htmlhint: {
            build: {
                options: {
                  'tag-pair': true,
                  'tagname-lowercase': true,
                  'attr-lowercase': true,
                  'attr-value-double-quotes': true,
                  'spec-char-escape': true,
                  'id-unique': true
                }, //options
                src: ['*.html']
            } //build
        }, //htmlhint
 
        watch: {
            options: { livereload: true }, // reloads browser on save
            scripts: {
                files: ['src/coffee/*.coffee'],
                tasks: ['coffee:compile', 'uglify']
            }, //scripts
            html: {
                files: ['src/*.html'],
                tasks: ['htmlhint:build']
            }, //html
        } //watch
    }) //initConfig
    grunt.registerTask('default', 'watch');
} //exports