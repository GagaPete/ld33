TILE_SIZE = 8

TICK_INTERVAL = 16

ZOOM = 3
WIDTH = 640
HEIGHT = 480

shownMovementHints = false
shownUsageHints = false

SPEED = .4

ACTION_KEYS = [88, 67, 32]

TILE_IDS =
    FEMALE_SPAWN: 258,
    MALE_SPAWN: 298,
    PATH: 338,
    FEMALE_PATH: 259,
    MALE_PATH: 299

Direction =
    LEFT: 1,
    UP: 2,
    RIGHT: 3,
    DOWN: 4

levels = [
    "appartement"
    "small_house",
    "resort",
    "bigger_house",
]

# I use a pixelated style, so scale pixelated
PIXI.SCALE_MODES.DEFAULT = PIXI.SCALE_MODES.NEAREST




#
class Entity
    constructor: (@id) ->
        @view = new PIXI.Container

    update: ->




class BitmapText extends PIXI.Container
    constructor: (@baseTexture, letters, @letterWidth, @letterHeight, @text) ->
        super

        x = 0
        y = 0
        for letter in letters
            @_letterTextures[letter] = new PIXI.Texture @baseTexture, new PIXI.Rectangle x, y, @letterWidth, @letterHeight
            x += @letterWidth
            if (x + @letterWidth) > @baseTexture.width
                x = 0
                y += @letterHeight

        @setText @text

    setText: (text) ->
        @removeChild child for child in @children

        x = 0
        y = 0
        for index in [0...text.length]
            letter = text[index]
            if @_letterTextures[letter]
                sprite = @addChild new PIXI.Sprite @_letterTextures[letter]
                sprite.position.set(x, y)

            x += @letterWidth

    getWidth: ->
        return @text.length * @letterWidth * @scale.x

    getHeight: ->
        return @letterHeight * @scale.y

    setColor: (color) ->
        child.tint = color for child in @children

    _letterTextures: {}




KittenCounter =
    incr: ->
        kittenCount = parseInt(localStorage.kittenCount)
        if isNaN kittenCount
            localStorage.kittenCount = 1
        else
            localStorage.kittenCount = kittenCount + 1

    get: ->
        kittenCount = parseInt localStorage.kittenCount
        kittenCount = 0 if isNaN kittenCount

        kittenCount



#
class Player extends Entity
    constructor: (id, @playState) ->
        super id
        # Ugly LDhack
        @textures = [
            new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(56, 93, 8, 11)
            new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(72, 93, 8, 11)
            new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(56, 109, 8, 11)
            new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(72, 109, 8, 11)
        ]
        @sprite = @view.addChild new PIXI.Sprite @textures[0]
        @sprite.position.set(-4, -7)

    update: (game) ->
        if @x == null
            @x = @view.position.x
            @y = @view.position.y

        if @kittenEatTime != null
            if @kittenEatTime > 10 and @kittenEatTime <= 30
                @sprite.texture = @textures[3]
            else
                @sprite.texture = @textures[2]
            if @kittenEatTime-- == 0
                @kittenEatTime = null
        else
            @x -= SPEED if game.keys[37] and !@playState.isSolid(@x - 4 - SPEED, @y - 4) and !@playState.isSolid(@x - 4 - SPEED, @y + 3)
            @y -= SPEED if game.keys[38] and !@playState.isSolid(@x - 4, @y - 4 - SPEED) and !@playState.isSolid(@x + 3, @y - 4 - SPEED)
            @x += SPEED if game.keys[39] and !@playState.isSolid(@x + 3 + SPEED, @y - 4) and !@playState.isSolid(@x + 3 + SPEED, @y + 3)
            @y += SPEED if game.keys[40] and !@playState.isSolid(@x - 4, @y + 3 + SPEED) and !@playState.isSolid(@x + 3, @y + 3 + SPEED)

            @view.scale.set 1, 1 if game.keys[37]
            @view.scale.set -1, 1 if game.keys[39]

            @sprite.texture = @textures[Math.floor(@lifetime / 40) % 2]

            @view.position.x = @x
            @view.position.y = @y

            ++@lifetime

            actionDown = false
            for key in ACTION_KEYS
                if game.keys[key]
                    actionDown = true

            if actionDown
                do @action


        bedTileIds = [
            # normal bed
            1446, 1447, 1486, 1487, 1526, 1527, 1566, 1567,

            # toddler bed
            1489, 1490, 1529, 1530, 1569, 1570,

            # double bed
            1460, 1461, 1462, 1463, 1500, 1501, 1502, 1503, 1540, 1541, 1542, 1543, 1580, 1581, 1582, 1583, 1300, 1301, 1302, 1340, 1341, 1342
        ]
        result = @playState.level.scanTiles Layer.WALL, Math.floor(@x / TILE_SIZE), Math.floor(@y / TILE_SIZE), 1, bedTileIds
        do @playState.showUsageHint if result.length > 0


    action: ->
        bedTileIds = [
            # normal bed
            1446, 1447, 1486, 1487, 1526, 1527, 1566, 1567,

            # toddler bed
            1489, 1490, 1529, 1530, 1569, 1570,

            # double bed
            1460, 1461, 1462, 1463, 1500, 1501, 1502, 1503, 1540, 1541, 1542, 1543, 1580, 1581, 1582, 1583, 1300, 1301, 1302, 1340, 1341, 1342
        ]
        result = @playState.level.scanTiles Layer.WALL, Math.floor(@x / TILE_SIZE), Math.floor(@y / TILE_SIZE), 1, bedTileIds

        if result.length > 0
            # check for the nearest tile
            nearestTile = null
            nearestDistance = 100
            for bedTile in result
                distance = Math.pow(Math.floor(@x / TILE_SIZE) - bedTile.x, 2) + Math.pow(Math.floor(@y / TILE_SIZE) - bedTile.y, 2)
                if (distance < nearestDistance)
                    nearestDistance = distance
                    nearestTile = bedTile

            if nearestTile != null
                x = nearestTile.x
                for y in [nearestTile.y...@playState.level.height]
                    tile = @playState.level.getTile Layer.WALL, x, y
                    if tile not in bedTileIds
                        # we reached a not bed tile, exit loop
                        break
                    else if tile in [1566, 1580, 1582]
                        @playState.wallTileByPos["#{x}_#{y}"].texture = game.tileset[1585]
                        @playState.wallTileByPos["#{x+1}_#{y}"].texture = game.tileset[1586]
                        @view.visible = false
                        @playState.won()
                        break
                    else if tile in [1567, 1581, 1583]
                        @playState.wallTileByPos["#{x-1}_#{y}"].texture = game.tileset[1585]
                        @playState.wallTileByPos["#{x}_#{y}"].texture = game.tileset[1586]
                        @view.visible = false
                        @playState.won()
                        break
                    else if tile == 1569
                        @playState.wallTileByPos["#{x}_#{y}"].texture = game.tileset[1571]
                        @playState.wallTileByPos["#{x+1}_#{y}"].texture = game.tileset[1572]
                        @view.visible = false
                        @playState.won()
                        break
                    else if tile == 1570
                        @playState.wallTileByPos["#{x-1}_#{y}"].texture = game.tileset[1571]
                        @playState.wallTileByPos["#{x}_#{y}"].texture = game.tileset[1572]
                        @view.visible = false
                        @playState.won()
                        break

        else
            # otherwise, try to eat KittenStrayState
            for entity in @playState.entities
                if entity instanceof Kitten
                    distance = Math.pow(Math.floor(@x / TILE_SIZE) - (entity.view.position.x / TILE_SIZE), 2) + Math.pow(Math.floor(@y / TILE_SIZE) - (entity.view.position.y / TILE_SIZE), 2)
                    if distance < 1.44
                        do KittenCounter.incr
                        @playState.despawn entity
                        @kittenEatTime = 50

    x: null
    y: null
    lifetime: 0
    kittenEatTime: null




# All the kittens, KITTENS!!!
KittenColor =
    GREY: 134,
    BROWN: 174

class KittenStrayState
    constructor: ->
        @duration = 900 + Math.floor(1800 * Math.random())

    update: (kitten) ->
        # first check if the cat is exhausted
        if @duration-- == 0
            kitten.state = new KittenSleepState

        # otherwise move
        else
            if @nextPos == null or (@nextPos.x == kitten.view.position.x and @nextPos.y == kitten.view.position.y)
                @nextPos = new PIXI.Point kitten.view.position.x, kitten.view.position.y
                directions = []
                directions.push [-TILE_SIZE, 0] if !kitten.playState.isSolid(kitten.view.position.x - 4 - TILE_SIZE, kitten.view.position.y - 4) and !kitten.playState.isSolid(kitten.view.position.x - 4 - TILE_SIZE, kitten.view.position.y + 3)
                directions.push [0, -TILE_SIZE] if !kitten.playState.isSolid(kitten.view.position.x - 4, kitten.view.position.y - 4 - TILE_SIZE) and !kitten.playState.isSolid(kitten.view.position.x + 3, kitten.view.position.y - 4 - TILE_SIZE)
                directions.push [TILE_SIZE, 0] if !kitten.playState.isSolid(kitten.view.position.x + 4, kitten.view.position.y - 4) and !kitten.playState.isSolid(kitten.view.position.x + 4, kitten.view.position.y + 3)
                directions.push [0, TILE_SIZE] if !kitten.playState.isSolid(kitten.view.position.x - 4, kitten.view.position.y + 4) and !kitten.playState.isSolid(kitten.view.position.x + 3, kitten.view.position.y + 4)

                keepDirection = false
                if Math.random() < 0.85 and @direction != null
                    for direction in directions
                        if direction[0] == @direction[0] and direction[1] == @direction[1]
                            keepDirection = true

                if not keepDirection
                    @direction = directions[Math.floor Math.random() * directions.length]

                if @direction
                    @nextPos.x += @direction[0]
                    @nextPos.y += @direction[1]
            else
                distanceX = (kitten.view.position.x - @nextPos.x)
                distanceX = Math.max -Kitten.SPEED, distanceX if distanceX < 0
                distanceX = Math.min Kitten.SPEED, distanceX if distanceX > 0
                distanceY = (kitten.view.position.y - @nextPos.y)
                distanceY = Math.max -Kitten.SPEED, distanceY if distanceY < 0
                distanceY = Math.min Kitten.SPEED, distanceY if distanceY > 0
                kitten.view.position.x -= distanceX
                kitten.view.position.y -= distanceY

                kitten.view.scale.set 1, 1 if kitten.view.position.x > @nextPos.x
                kitten.view.scale.set -1, 1 if kitten.view.position.x < @nextPos.x

            # LD is hackish!!
            kitten.sprite.texture = window.game.tileset[kitten.color + Math.floor(@duration / 17) % 2]

    nextPos: null
    duration: null
    direction: null

class KittenSleepState
    constructor: ->
        @duration = 300 + Math.floor(700 * Math.random())

    update: (kitten) ->
        if @duration-- == 0
            kitten.state = new KittenStrayState
        else
            kitten.sprite.texture = window.game.tileset[kitten.color + 2]

    duration: null

class Kitten extends Entity
    constructor: (id, @playState) ->
        super
        @sprite = @view.addChild new PIXI.Sprite window.game.tileset[134]
        @sprite.position.set -4, -4
        #@sprite.anchor.set 0.5


        @state = new KittenStrayState

    update: (game) ->
        @state.update @


    state: null

    sprite: null

    @SPEED: 0.4
    color: KittenColor.GREY




# Adults
AdultGender =
    FEMALE: [
        143, 72,
        159, 72,
        143, 96,
        159, 96,
        143, 120,
        159, 120,
        143, 144,
        159, 144
    ],
    MALE: [
        111, 72,
        127, 72,
        111, 96,
        127, 96,
        111, 120,
        127, 120,
        111, 144,
        127, 144
    ]

class Adult extends Entity
    constructor: (id, @playState, @x, @y, @gender) ->
        super
        @textures = [
            new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(@gender[0], @gender[1], 10, 16)
            new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(@gender[2], @gender[3], 10, 16)
            new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(@gender[4], @gender[5], 10, 16)
            new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(@gender[6], @gender[7], 10, 16)
            new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(@gender[8], @gender[9], 10, 16)
            new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(@gender[10], @gender[11], 10, 16)
            new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(@gender[12], @gender[13], 10, 16)
            new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(@gender[14], @gender[15], 10, 16)
        ]
        @sprite = @view.addChild new PIXI.Sprite @textures[0]
        @sprite.position.set -5, -12

        @bubble = @view.addChild new PIXI.Sprite new PIXI.Texture PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(74, 73, 12, 14)
        @bubble.position.set -3, -27
        @bubble.visible = false

        @path = []
        do =>
            openNodes = [[Math.floor(@x / TILE_SIZE), Math.floor(@y / TILE_SIZE)]]
            closedNodes = []
            isClosed = (x, y) ->
                result = false
                for node in closedNodes
                    if node[0] == x and node[1] == y
                        result = true
                        break
                result

            isPath = (tile) =>
                valid = [TILE_IDS.PATH, TILE_IDS.MALE_SPAWN, TILE_IDS.FEMALE_SPAWN]
                if @gender == AdultGender.FEMALE
                    valid.push TILE_IDS.FEMALE_PATH
                else
                    valid.push TILE_IDS.MALE_PATH

                tile in valid

            while openNodes.length > 0
                [x, y] = do openNodes.pop
                openNodes.push [x - 1, y] if isPath(@playState.level.getTile(Layer.OBJECT, x - 1, y)) and !isClosed(x - 1, y)
                openNodes.push [x + 1, y] if isPath(@playState.level.getTile(Layer.OBJECT, x + 1, y)) and !isClosed(x + 1, y)
                openNodes.push [x, y - 1] if isPath(@playState.level.getTile(Layer.OBJECT, x, y - 1)) and !isClosed(x, y - 1)
                openNodes.push [x, y + 1] if isPath(@playState.level.getTile(Layer.OBJECT, x, y + 1)) and !isClosed(x, y + 1)

                @path.push [x, y]

                closedNodes.push [x, y]

        @currentNode = [Math.floor(@x / TILE_SIZE), Math.floor(@y / TILE_SIZE)]


    update: (game) ->
        if @pauseTime == 0
            if @nextNode == null or (@x == @nextNode[0] * TILE_SIZE and @y == @nextNode[1] * TILE_SIZE)
                @previousNode = @currentNode
                @currentNode = @nextNode if @nextNode != null
                # chose a next node
                isNode = (x, y) =>
                    result = false
                    for node in @path
                        if node[0] == x and node[1] == y
                            result = true
                            break
                    result

                tileX = Math.floor(@x / TILE_SIZE)
                tileY = Math.floor(@y / TILE_SIZE)

                options = []
                options.push [tileX - 0.5, tileY + 0.5] if isNode tileX - 1, tileY
                options.push [tileX + 1.5, tileY + 0.5] if isNode tileX + 1, tileY
                options.push [tileX + 0.5, tileY - 0.5] if isNode tileX, tileY - 1
                options.push [tileX + 0.5, tileY + 1.5] if isNode tileX, tileY + 1

                if options.length == 1
                    @nextNode = options[0]
                    @pauseTime = 60 + Math.floor 240 * Math.random()

                else if options.length > 1
                    if @previousNode != null
                        options = options.filter (v) =>
                            v[0] != @previousNode[0] or v[1] != @previousNode[1]

                    @nextNode = options[Math.floor options.length * Math.random()]



            else
                distanceX = @x - @nextNode[0] * TILE_SIZE
                distanceX = Math.max -Adult.SPEED, distanceX if distanceX < 0
                distanceX = Math.min Adult.SPEED, distanceX if distanceX > 0
                distanceY = @y - @nextNode[1] * TILE_SIZE
                distanceY = Math.max -Adult.SPEED, distanceY if distanceY < 0
                distanceY = Math.min Adult.SPEED, distanceY if distanceY > 0
                @x -= distanceX
                @y -= distanceY

                @direction = Direction.LEFT if @nextNode[0] < @currentNode[0]
                @direction = Direction.RIGHT if @nextNode[0] > @currentNode[0]
                @direction = Direction.UP if @nextNode[1] < @currentNode[1]
                @direction = Direction.DOWN if @nextNode[1] > @currentNode[1]

        else
            @pauseTime--

        @view.position.x = @x
        @view.position.y = @y

        base = (if @direction == Direction.LEFT
            2
        else if @direction == Direction.RIGHT
            4
        else if @direction == Direction.UP
            6
        else
            0)
        @sprite.texture = @textures[base + Math.floor(Date.now() / 300) % 2]

        if @playState._focus != null and (@playState.level.isLineOfSightFree @x / TILE_SIZE, @y / TILE_SIZE, @playState._focus.view.x / TILE_SIZE, @playState._focus.view.y / TILE_SIZE)
            angle = Math.atan2 @playState._focus.view.y - @y, @playState._focus.view.x - @x

            isVisible = false
            if @direction == Direction.LEFT and (angle < Math.atan2(-1, -2) or angle > Math.atan2(1, -2))
                isVisible = true
            else if @direction == Direction.RIGHT and (angle > Math.atan2(-1, 2) and angle < Math.atan2(1, 2))
                isVisible = true
            else if @direction == Direction.UP and (angle > Math.atan2(-2, 1) and angle < Math.atan2(-2, -1))
                isVisible = true
            else if @direction == Direction.DOWN and (angle > Math.atan2(2, 1) and angle < Math.atan2(2, -1))
                isVisible = true

            if (Math.pow(@playState._focus.view.y - @y, 2) + Math.pow(@playState._focus.view.x - @x, 2)) > Math.pow(90, 2)
                isVisible = false

            @heatupTime++
            if isVisible
                if @heatupTime > 15
                    @bubble.visible = true
                    do @playState.loose
            else
                @heatupTime = 0
        else
            @heatupTime = 0




    previousNode: null
    currentNode: null
    nextNode: null
    direction: null
    @SPEED: 0.3
    pauseTime: 0
    heatupTime: 0




#
#
#
Layer =
    FLOOR: 1,
    WALL: 2,
    OBJECT: 3

class Level
    constructor: (@width, @height, @floorData, @wallData, @objectsData) ->

    getTile: (layer, x, y) ->
        result = null

        if layer == Layer.FLOOR
            data = @floorData
        else if layer == Layer.WALL
            data = @wallData
        else if layer == Layer.OBJECT
            data = @objectsData

        if data != null and x >= 0 and x < @width and y >= 0 and y < @height
            result = data[x + y * @width]

        result

    scanTiles: (layer, x, y, range, forIds) ->
        result = []

        if layer == Layer.FLOOR
            data = @floorData
        else if layer == Layer.WALL
            data = @wallData
        else if layer == Layer.OBJECT
            data = @objectsData

        beginX = Math.max 0, x - range
        endX = Math.min @width, x + range

        beginY = Math.max 0, y - range
        endY = Math.min @height, y + range

        for x in [beginX..endX]
            for y in [beginY..endY]
                id = data[x + y * @width]
                if id in forIds
                    result.push
                        x: x,
                        y: y,
                        id: id

        result

    isLineOfSightFree: (fromX, fromY, toX, toY) ->
        result = true

        distanceX = toX - fromX
        distanceY = toY - fromY

        prevX = -1
        prevY = -1

        if Math.abs(distanceX) > Math.abs(distanceY)
            for x in [fromX..toX]
                y = fromY + distanceY * (x - fromX) / distanceX
                tileX = Math.floor x
                tileY = Math.floor y
                if prevX != tileX or prevY != tileY
                    prevX = tileX
                    prevY = tileY
                    tile = @getTile Layer.WALL, tileX, tileY
                    if tile != null and tile >= 0
                        result = false
                        break
                    tile = @getTile Layer.FLOOR, tileX, tileY
                    if tile != null and tile == 291
                        result = false
                        break
        else
            for y in [fromY..toY]
                x = fromX + distanceX * (y - fromY) / distanceY
                tileX = Math.floor x
                tileY = Math.floor y
                if prevX != tileX or prevY != tileY
                    prevX = tileX
                    prevY = tileY
                    tile = @getTile Layer.WALL, tileX, tileY
                    if tile != null and tile >= 0
                        result = false
                        break
                    tile = @getTile Layer.FLOOR, tileX, tileY
                    if tile != null and tile == 291
                        result = false
                        break


        result


    @fromCsvs: (url) ->
        result = null

        if PIXI.loader.resources[url + "_floor.csv"] and PIXI.loader.resources[url + "_walls.csv"] and PIXI.loader.resources[url + "_objects.csv"]
            floorData = []
            wallData = []
            objectsData = []

            resource = PIXI.loader.resources[url + "_floor.csv"]
            rows = resource.data.split "\n"
            height = rows.length
            width = (rows[0].split ",").length
            for row in rows
                floorData = floorData.concat row.split ","

            resource = PIXI.loader.resources[url + "_walls.csv"]
            rows = resource.data.split "\n"
            height = rows.length
            width = (rows[0].split ",").length
            for row in rows
                wallData = wallData.concat row.split ","

            resource = PIXI.loader.resources[url + "_objects.csv"]
            rows = resource.data.split "\n"
            height = rows.length
            width = (rows[0].split ",").length
            for row in rows
                objectsData = objectsData.concat row.split ","

            floorData = floorData.map (v) -> parseInt(v, 10)
            wallData = wallData.map (v) -> parseInt(v, 10)
            objectsData = objectsData.map (v) -> parseInt(v, 10)

            result = new Level width, height, floorData, wallData, objectsData

        result




#
#
#
class LoadingScene
    constructor: (@nextScene) ->

    enter: (game) ->
        @intro = PIXI.Sprite.fromImage "img/intro.png"
        @intro.scale.set 4
        game.stage.addChild @intro

        #PIXI.loader.add "img/gagapete.png"
        PIXI.loader.add "tileset", "img/tileset.png"
        PIXI.loader.add "outro", "img/outro.png"
        for level in levels
            PIXI.loader.add "lvl/" + level + "_floor.csv"
            PIXI.loader.add "lvl/" + level + "_walls.csv"
            PIXI.loader.add "lvl/" + level + "_objects.csv"
        PIXI.loader.on "complete", => @completed = true
        do PIXI.loader.load

        @timeEntered = Date.now()

    leave: (game) ->
        game.stage.removeChild @intro

    update: (game) ->
        if @completed and (Date.now() - @timeEntered) > 2000
            @_prepareTileset game
            game.changeScene @nextScene

    _prepareTileset: (game) ->
        for y in [0...PIXI.loader.resources.tileset.texture.height / TILE_SIZE]
            for x in [0...PIXI.loader.resources.tileset.texture.width / TILE_SIZE]
                game.tileset.push new PIXI.Texture(PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE))

    timeEntered: 0
    completed: false
    nextScene: null




#
#
#
class NextScene
    enter: (game) ->
        current = parseInt(localStorage.currentLevel)
        if isNaN(current) or current <= 0
            current = localStorage.currentLevel = 0

        if current >= levels.length
            localStorage.currentLevel = 0
            game.changeScene new OutroScene
        else
            level = Level.fromCsvs "lvl/" + levels[current]
            game.changeScene new PlayScene level

    leave: (game) ->
    update: (game) ->




#
#
#
class OutroScene
    enter: (game) ->
        @sprite = new PIXI.Sprite PIXI.loader.resources.outro.texture
        @sprite.scale.set 4
        game.stage.addChild @sprite

    leave: (game) ->
        game.stage.removeChild @sprite

    update: (game) ->
        actionDown = false
        for key in ACTION_KEYS
            if game.keys[key]
                actionDown = true

        if actionDown
            if @wasActionUp
                game.changeScene new NextScene
        else
            @wasActionUp = true

    wasActionUp: false



#
#
#
class PlayScene
    constructor: (@level) ->
        @camera = new PIXI.Container
        @floorTiles = new PIXI.Container
        @wallTiles = new PIXI.Container
        @camera.addChild @floorTiles
        @camera.addChild @wallTiles
        @camera.scale.set ZOOM
        @camera.alpha = 0
        @fadein = 30

        @movementKeys = new PIXI.Sprite new PIXI.Texture(PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(125, 171, 41, 23))
        @movementKeys.scale.set ZOOM
        @movementKeys.position.set (640 - 41 * ZOOM) / 2, 480 - 27 * ZOOM
        @movementKeys.visible = !shownMovementHints
        shownMovementHints = true

        @useKey = new PIXI.Sprite new PIXI.Texture(PIXI.loader.resources.tileset.texture.baseTexture, new PIXI.Rectangle(129, 198, 34, 11))
        @useKey.scale.set ZOOM
        @useKey.position.set (640 - 34 * ZOOM) / 2, 480 - 21 * ZOOM
        @useKey.visible = !shownUsageHints
        @useKey.alpha = 0

        for x in [0...@level.width]
            for y in [0...@level.height]
                id = @level.floorData[y * @level.width + x]
                if id >= 0
                    sprite = new PIXI.Sprite game.tileset[id]
                    sprite.x = x * TILE_SIZE
                    sprite.y = y * TILE_SIZE
                    #sprite.tint = 0x333349
                    sprite.scale.set 1
                    @floorTiles.addChild sprite
                    @floorTileByPos["#{x}_#{y}"] = sprite

                id = @level.wallData[y * @level.width + x]
                if id >= 0
                    sprite = new PIXI.Sprite game.tileset[id]
                    sprite.x = x * TILE_SIZE
                    sprite.y = y * TILE_SIZE
                    sprite.scale.set 1
                    #sprite.tint = 0x333349
                    @wallTiles.addChild sprite
                    @wallTileByPos["#{x}_#{y}"] = sprite

                if @level.objectsData[y * @level.width + x] == 321
                    @_focus = @_spawn Player, (x + 0.5) * TILE_SIZE, (y + 0.5) * TILE_SIZE

                if @level.objectsData[y * @level.width + x] == 134
                    @_spawn Kitten, (x + 0.5) * TILE_SIZE, (y + 0.5) * TILE_SIZE

                if @level.objectsData[y * @level.width + x] == 174
                    kitten = @_spawn Kitten, (x + 0.5) * TILE_SIZE, (y + 0.5) * TILE_SIZE
                    kitten.color = KittenColor.BROWN

                if @level.objectsData[y * @level.width + x] == TILE_IDS.MALE_SPAWN
                    @_spawn Adult, (x + 0.5) * TILE_SIZE, (y + 0.5) * TILE_SIZE, AdultGender.MALE

                if @level.objectsData[y * @level.width + x] == TILE_IDS.FEMALE_SPAWN
                    @_spawn Adult, (x + 0.5) * TILE_SIZE, (y + 0.5) * TILE_SIZE, AdultGender.FEMALE


    enter: (game) ->
        game.stage.addChild @camera
        game.stage.addChild @movementKeys
        game.stage.addChild @useKey

    leave: (game) ->
        @wallTiles.destroy()
        @floorTiles.destroy()
        game.stage.removeChild @camera
        game.stage.removeChild @movementKeys
        game.stage.removeChild @useKey

    update: (game) ->
        if @fadein != null
            @camera.alpha = 1 - (@fadein / 30)
            if @fadein-- == 0
                @fadein = null
        else
            if @fadeout == null
                for entity in @entities
                    entity.update game if entity
            else
                @fadeout--

                if @fadeout == 0
                    game.changeScene new NextScene

                else
                    progress = Math.floor(255 * @fadeout / 60)

                    @camera.alpha = progress / 255

        if @_focus
            @camera.position.set @_focus.view.position.x * -ZOOM + (WIDTH / 2), @_focus.view.position.y * -ZOOM + (HEIGHT / 2)

        # movement key hint management
        if @movementKeysFadeout == null and (game.keys[37] or game.keys[38] or game.keys[39] or game.keys[40])
            @movementKeysFadeout = 90

        if @movementKeysFadeout != null
            if @movementKeysFadeout < 30
                @movementKeys.alpha = @movementKeysFadeout / 30

            if @movementKeysFadeout-- == 0
                @movementKeysFadeout = null
                @movementKeys.visible = false

        # use key hint management
        actionDown = false
        for key in ACTION_KEYS
            if game.keys[key]
                actionDown = true

        if @useKey.visible and @useKey.alpha > 0 and @useKeyFadeOut == null and actionDown
            @useKeyFadeOut = 30

        if @useKeyFadeIn != null
            @useKey.alpha = 1 - (@useKeyFadeIn / 30)

            if @useKeyFadeIn-- == 0
                @useKeyFadeIn = null

        if @useKeyFadeOut != null
            @useKey.alpha = @useKeyFadeOut / 30

            if @useKeyFadeOut-- == 0
                @useKeyFadeOut = null
                @useKey.visible = false

    isSolid: (x, y) ->
        result = true
        x = Math.floor x / TILE_SIZE
        y = Math.floor y / TILE_SIZE
        if x >= 0 and x < @level.width and y >= 0 and y < @level.height
            result = @level.wallData[y * @level.width + x] != -1

        result

    won: () ->
        @fadeout = 60

        if isNaN(parseInt(localStorage.currentLevel))
            localStorage.currentLevel = 1
        else
            localStorage.currentLevel = parseInt(localStorage.currentLevel) + 1

    loose: () ->
        @fadeout = 60

    showUsageHint: () ->
        if not @movementKeys.visible and not shownUsageHints
            @useKeyFadeIn = 30
            shownUsageHints = true

    _spawn: (entityType, x, y, extra...) ->
        entity = new entityType @_autoId++, @, x, y, extra...
        entity.view.position.set x, y
        @camera.addChild entity.view

        @entities.push entity

        entity

    despawn: (entity) ->
        index = @entities.indexOf entity
        if index != -1
            @camera.removeChild entity.view
            @entities.splice index, 1

    level: null

    entities: []

    _focus: null

    camera: null

    floorTiles: []
    floorTileByPos: {}
    wallTiles: []
    wallTileByPos: {}

    _autoId: 0
    fadein: null
    fadeout: null

    movementKeys: null
    movementKeysFadeout: null

    useKey: null
    useKeyFadeIn: null
    useKeyFadeOut: null



#
#
#
class Game
    stage: null

    renderer: null

    tileset: []

    constructor: (@scene) ->

    setup: ->
        @stage = new PIXI.Container
        @renderer = PIXI.autoDetectRenderer WIDTH, HEIGHT, null

        document.body.appendChild @renderer.view

        window.addEventListener "keydown", @handleKeydown.bind @
        window.addEventListener "keyup", @handleKeyup.bind @
        window.addEventListener "blur", @handleBlur.bind @

        @scene.enter @

        @lastUpdate = Date.now() - TICK_INTERVAL
        do @update

    changeScene: (scene) ->
        if @scene
            @scene.leave @

        @scene = scene
        @scene.enter @

    handleKeydown: (event) ->
        @keys[event.which] = true

    handleKeyup: (event) ->
        @keys[event.which] = false

    handleBlur: (event) ->
        @keys = new Uint8Array 256

    keys: new Uint8Array 256

    update: ->
        requestAnimationFrame @update.bind @

        @lastUpdate = Math.max @lastUpdate, Date.now() - 1000

        while @lastUpdate < Date.now()
            @lastUpdate += TICK_INTERVAL
            @scene.update @

        @renderer.render @stage

# Run the game when the page is loaded

window.game = new Game new LoadingScene new NextScene
addEventListener "load", ->
    do window.game.setup
